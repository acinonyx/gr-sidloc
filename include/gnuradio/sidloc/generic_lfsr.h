/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef GENERIC_LFSR_H
#define GENERIC_LFSR_H

#include <gnuradio/digital/lfsr.h>
#include <gnuradio/sidloc/api.h>
#include <gnuradio/sidloc/code.h>

namespace gr {
namespace sidloc {
class SIDLOC_API generic_lfsr : public code
{
private:
    const size_t m_degree;
    size_t m_cnt;
    digital::lfsr m_lfsr;

public:
    static code::sptr make_shared(uint64_t mask, uint64_t seed, size_t degree);

    generic_lfsr(uint64_t mask, uint64_t seed, size_t degree);
    ~generic_lfsr();

    size_t length() const;

    void next(std::vector<bool>& x);

    void reset();

    void period(std::vector<bool>& seq, size_t channel);
};

} // namespace sidloc
} // namespace gr


#endif // GENERIC_LFSR_H

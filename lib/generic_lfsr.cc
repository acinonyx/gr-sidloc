/* -*- c++ -*- */
/*
 * gr-sidloc: GNU Radio OOT module for operation and experimentation with the
 * Spacecraft Identification and Localization (SIDLOC) system.
 *
 * Copyright 2022 Libre Space Foundation <https://libre.space>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/sidloc/generic_lfsr.h>

namespace gr {
namespace sidloc {

/**
 * @brief Returns a shared pointer to a generic_lfsr object
 *
 * @param mask - polynomial coefficients representing the
 *             locations of feedback taps from a shift register
 *             which are xor'ed together to form the new high
 *             order bit.
 *
 *             Some common masks might be:
 *              x^4 + x^3 + x^0 = 0x19, K=3
 *              x^5 + x^3 + x^0 = 0x29, K=4
 *              x^6 + x^5 + x^0 = 0x61, K=5
 *
 * @param seed - the initialization vector placed into the
 *             register during initialization. Low order bit
 *             corresponds to x^0 coefficient -- the first to be
 *             shifted as output.
 * @param degree the degree of the shift register. This should be equals the number of
 * memory stages. Pay attention that this is +1 from the reg_len parameter that GNU Radio
 * uses
 *
 *
 * @return code::sptr a shared pointer to a generic_lfsr object
 */
code::sptr generic_lfsr::make_shared(uint64_t mask, uint64_t seed, size_t degree)
{
    return code::sptr(new generic_lfsr(mask, seed, degree - 1));
}

/**
 * @brief Construct a new generic lfsr::generic lfsr object
 *
 * @param mask - polynomial coefficients representing the
 *             locations of feedback taps from a shift register
 *             which are xor'ed together to form the new high
 *             order bit.
 *
 *             Some common masks might be:
 *              x^4 + x^3 + x^0 = 0x19, K=3
 *              x^5 + x^3 + x^0 = 0x29, K=4
 *              x^6 + x^5 + x^0 = 0x61, K=5
 *
 * @param seed - the initialization vector placed into the
 *             register during initialization. Low order bit
 *             corresponds to x^0 coefficient -- the first to be
 *             shifted as output.
 * @param degree the degree of the shift register. This should be equals the number of
 * memory stages. Pay attention that this is +1 from the reg_len parameter that GNU Radio
 * uses
 */
generic_lfsr::generic_lfsr(uint64_t mask, uint64_t seed, size_t degree)
    : code(1), m_degree(degree), m_cnt(0), m_lfsr(mask, seed, degree - 1)
{
}

generic_lfsr::~generic_lfsr() {}

size_t generic_lfsr::length() const { return (1 << m_degree) - 1; }

void generic_lfsr::next(std::vector<bool>& x)
{
    x[0] = m_lfsr.next_bit();
    m_cnt++;
    if (m_cnt == length()) {
        m_lfsr.reset();
    }
}

void generic_lfsr::reset() { m_lfsr.reset(); }

void generic_lfsr::period(std::vector<bool>& seq, size_t channel)
{
    if (channel > 0) {
        throw std::invalid_argument("generic_lfsr: Code supports 1 channel");
    }
    digital::lfsr x = m_lfsr;
    x.reset();
    for (size_t i = 0; i < length(); i++) {
        seq.push_back(x.next_bit());
    }
}


} // namespace sidloc
} // namespace gr
